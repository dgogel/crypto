# slack_get_address_balance
Gets btc balance of an address, formatted for Slack

#### example calls:
##### cli
`a8 invoke crypto_btc.slack_get_address_balance  '{"address":"3D2oetdNuZUqQHPJmcMDDHYoqkyNVsFk9r"}'`

##### slack
`/a8 invoke crypto_btc.slack_get_address_balance(address:3D2oetdNuZUqQHPJmcMDDHYoqkyNVsFk9r)`


## input
`address` is a btc address
```
{
    address : string
}
```

## output

```
{
  "response_type": "in_channel",
  "text": `${btc} btc`
} 
```