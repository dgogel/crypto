const fdk=require('@fnproject/fdk')
const a8=require('@autom8/js-a8-fdk')

fdk.handle(function(input){

  return a8.crypto_btc.get_address_balance(input)
    .then((result) => {
      if(!result.btc){
        return {
          "response_type": "ephemeral",
          "text": `Couldn't find BTC for this account!`
        }
      }

      return {
        "response_type": "in_channel",
        "text": `${result.btc} btc`
      }
     })
})
