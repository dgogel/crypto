# get_supply
Gets the count of total btc

#### example calls:
##### cli
`a8 invoke crypto_btc.get_supply`

## input
none or 
`{}`

## output
count is number of btc mined up to this point
```
 {
   count: int
 }   
```