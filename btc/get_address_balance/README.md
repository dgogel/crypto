# get_address_balance
Gets btc balance of an address

#### example calls:
##### cli
`a8 invoke crypto_btc.get_address_balance  '{"address":"3D2oetdNuZUqQHPJmcMDDHYoqkyNVsFk9r"}'`

## input
`address` is a btc address
```
{
    address : string
}
```

## output
`btc` is the amount of btc currently associated with the address  

```
 { 
    btc: int
 }   
```