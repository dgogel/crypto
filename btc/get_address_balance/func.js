const fdk=require('@fnproject/fdk')
const a8=require('@autom8/js-a8-fdk')
const blockchain= require('blockchain.info')

fdk.handle(function(input){

  if(!input.address){
    return {
      error : 'no address entered'
    }
  }

  const address = input.address

  return blockchain.blockexplorer.getBalance(address)
    .then((result)=>{
      return {
        btc :  result[address].final_balance/100000000
      }
    })
})
