const fdk=require('@fnproject/fdk')
const a8=require('@autom8/js-a8-fdk')
const blockchain= require('blockchain.info')

fdk.handle(function(input){

  return blockchain.statistics.get({}).then((result)=>{
    return {
      market_cap_usd : result.market_price_usd * result.totalbc / 100000000
    }

  })
})
