# get_market_cap_usd
gets market cap of bitcoin in usd 

#### example calls:
##### cli
`a8 invoke crypto_btc.get_market_cap_usd`

## input
`{}` or none

## output
`market_cap_usd` market cap of usd in btc  

```
 { 
    market_cap_usd: int
 }   
```