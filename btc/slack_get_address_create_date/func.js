const fdk=require('@fnproject/fdk')
const a8=require('@autom8/js-a8-fdk')

fdk.handle(function(input){
  return a8.crypto_btc.get_address_create_date(input)
    .then((result) => {
      if(!result.unixTime){
        return {
          "response_type": "ephemeral",
          "text": `Nothing found for this address!`
        }
      }

      return {
        "response_type": "in_channel",
        "text": `<!date^${result.unixTime}^First Transaction {date_num} {time_secs}| ${result.niceTime}>`
      }
    })
})

