# slack_get_address_create_date
Gets the date of the first transaction an address made, formatted for Slack

#### example calls:
##### cli
`a8 invoke crypto_btc.slack_get_address_create_date  '{"address":"3D2oetdNuZUqQHPJmcMDDHYoqkyNVsFk9r"}'`

##### slack
`/a8 invoke crypto_btc.slack_get_address_create_date(address:3D2oetdNuZUqQHPJmcMDDHYoqkyNVsFk9r)`

## input
`address` is a btc address
```
{
  address : string
}
```

## output
Text is nicely formatted for Slack date so it will print nicely

```
{
  "response_type": "in_channel",
  "text": `<!date^${unixTime}^First Transaction {date_num} {time_secs}| ${result.niceTime}>`
} 
```