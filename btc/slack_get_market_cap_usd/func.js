const fdk=require('@fnproject/fdk')
const a8=require('@autom8/js-a8-fdk')

fdk.handle(function(input){
  return a8.crypto_btc.get_market_cap_usd(input)
    .then((result) => {
      if(!result.market_cap_usd){
        return {
          "response_type": "ephemeral",
          "text": `Something went wrong!!`
        }
      }

      return {
        "response_type": "in_channel",
        "text": `$${numberWithCommas(result.market_cap_usd)} dollars`
      }
     })
})

const numberWithCommas = (x) => {
  const numSplit = x.toString().split('.')
  numSplit[0] =  numSplit[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",")

  return numSplit.join('.')
}
