# slack_get_market_cap_usd
gets market cap of bitcoin in usd, formatted for Slack

#### example calls:
##### cli
`a8 invoke crypto_btc.slack_get_market_cap_usd `

##### slack
`/a8 invoke crypto_btc.slack_get_market_cap_usd()`

## input
`{}` or none

## output
`market_cap_usd` market cap of usd in btc  

```
{
  "response_type": "in_channel",
  "text": `$${market_cap_usd}`
}
```

