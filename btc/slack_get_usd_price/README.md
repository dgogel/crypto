# slack_get_usd_price
Gets price of an amount of bitcoin in usd, formatted for Slack 

#### example calls:
##### cli
`a8 invoke crypto_btc.slack_get_usd_price `

##### slack
`/a8 invoke crypto_btc.slack_get_usd_price()`


## input
`count` is number of btc, it defaults to 1
```
{
    count : int
}
```

## output
`usd` is the amount in usd of the btc   

```
{
  "response_type": "in_channel",
  "text": `$${usd}`
}   
```