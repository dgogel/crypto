const fdk=require('@fnproject/fdk');
const a8=require('@autom8/js-a8-fdk')

fdk.handle(function(input){

  return a8.crypto_btc.get_usd_price(input)
    .then((result) => {
      if(!result.usd){
        return {
          "response_type": "ephemeral",
          "text": `something went wrong with this function!`
        }
      }

      return {
        "response_type": "in_channel",
        "text": `$${result.usd}`
      }
    })
})
