const fdk=require('@fnproject/fdk')
const a8=require('@autom8/js-a8-fdk')
const blockchain= require('blockchain.info')

fdk.handle(function(input){
  if(!input.address){
    return {
      error : 'no address entered'
    }
  }

  const address = input.address

  return blockchain.blockexplorer.getAddress(address)
    .then((result) => {
      return result.txs.reduce((acc, tx) => {
        return tx.time < acc ? tx.time : acc
      }, Infinity)
    })
    .then((unixTime) => {
      return {
        unixTime: unixTime,
        niceTime: timeConverter(unixTime)
      }
    })

})

const timeConverter = (UNIX_timestamp) => {
  const a = new Date(UNIX_timestamp * 1000)
  const months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
  const year = a.getFullYear()
  const month = months[a.getMonth()]
  const date = a.getDate()
  const hour = a.getHours()
  const min = a.getMinutes()
  const sec = a.getSeconds()
  return date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec
}