const fdk=require('@fnproject/fdk')
const a8=require('@autom8/js-a8-fdk')

fdk.handle(function(input){
  return a8.crypto_btc.get_supply(input)
    .then((result) => {
      if(!result.count){
        return {
          "response_type": "ephemeral",
          "text": `Something went wrong!!`
        }
      }

      return {
        "response_type": "in_channel",
        "text": `Total BTC mined: ${numberWithCommas(result.count)}`
      }
     })
})

const numberWithCommas = (x) => {
  const numSplit = x.toString().split('.')
  return numSplit[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}
