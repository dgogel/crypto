# slack_get_supply
Gets the count of total btc, formatted for Slack

#### example calls:
##### cli
`a8 invoke crypto_btc.slack_get_supply `

##### slack
`/a8 invoke crypto_btc.slack_get_supply()`

## input
none or 
`{}`

## output
count is number of btc mined up to this point
```
{
  "response_type": "in_channel",
  "text": `Total btc mined: ${count}`
}
```