# get_usd_price
Gets price of an amount of bitcoin in usd 

#### example calls:
##### cli
`a8 invoke crypto_btc.get_usd_price`

## input
`btc` is number of btc, it defaults to 1
```
{
    btc : int
}
```

## output
`usd` is the amount in usd of the btc   

```
 { 
    usd: int,  
 }   
```