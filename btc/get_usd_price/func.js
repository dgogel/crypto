const fdk=require('@fnproject/fdk');
const a8=require('@autom8/js-a8-fdk')
const blockchain= require('blockchain.info')

fdk.handle(function(input){

  let btc = 1
  if (input.btc) {
    btc = input.btc
  }

  return blockchain.statistics.get({stat:'market_price_usd'})
    .then((result)=>{
      return {
        usd : result * btc
      }
    })
})
