# Namespace: crypto

## **Currency:** ETH

### [get_usd_price](eth/get_usd_price)
    slack: /a8 crypto_eth.get_usd_price
### [get_gas_price](eth/get_gas_price)
    slack: /a8 crypto_eth.get_gas_price
### [get_address_balance](eth/get_address_balance)
    slack: /a8 crypto_eth.get_address_balance(address:0xB8c77482e45F1F44dE1745F52C74426C631bDD52)
### [get_supply](eth/get_supply)
    slack: /a8 crypto_eth.get_supply
### [get_token_supply](eth/get_token_supply)
    slack: /a8 crypto_eth.get_token_supply(address:0xB8c77482e45F1F44dE1745F52C74426C631bDD52)
### [get_address_first_use_date](eth/get_address_first_use_date)
    slack: /a8 crypto_eth.get_address_first_use_date(address:0xB8c77482e45F1F44dE1745F52C74426C631bDD52)         broken
### [get_address_transactions](eth/get_address_transactions)
    slack: /a8 crypto_eth.get_address_transactions(address:0xB8c77482e45F1F44dE1745F52C74426C631bDD52)

## **Currency:** BTC

### [get_usd_price](btc/get_usd_price)
    slack: /a8 crypto_btc.get_usd_price
### [get_supply](btc/get_supply)    
    slack: /a8 crypto_btc.get_supply
### [get_market_cap_usd](btc/get_market_cap_usd)
    slack: /a8 crypto_btc.get_market_cap_usd
### [get_address_balance](btc/get_address_balance)
    slack: /a8 crypto_btc.get_address_balance(address:0xB8c77482e45F1F44dE1745F52C74426C631bDD52)
### [get_address_create_date](btc/get_address_create_date)
    slack: /a8 crypto_btc.get_address_create_date(address:3D2oetdNuZUqQHPJmcMDDHYoqkyNVsFk9r)
### [get_transaction_result](btc/get_transaction_result)
    slack: /a8 crypto_btc.get_transaction_result(transaction:d14fbf1b36222c7e7111a877b2f7e948ec65825da88da7a1683cbdf55990d369)
