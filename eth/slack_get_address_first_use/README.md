# slack_get_address_first_use_date
Gets the date of the first time an address made a transaction, formatted for Slack

#### example calls:
##### cli
`a8 invoke crypto_eth.slack_get_address_first_use_date '{"address":"0xB8c77482e45F1F44dE1745F52C74426C631bDD52"}'`

##### slack
`/a8 invoke crypto_eth.slack_get_address_first_use_date(address:0xB8c77482e45F1F44dE1745F52C74426C631bDD52)`


## input
`address` is an eth address
```
{
    address : string
}
```

## output
`text` is nicely formatted for slack dates

```
{
 "response_type": "in_channel",
  "text": `<!date^${unixTime}^First Transaction {date_num} {time_secs}| ${niceTime}>`
} 
```