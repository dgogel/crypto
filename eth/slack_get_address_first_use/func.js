const fdk=require('@fnproject/fdk');
const a8=require('@autom8/js-a8-fdk')

fdk.handle(function(input){
  let address = ""
  if (!input.address) {
    return {message: 'please include an account'}
  }

  return a8.crypto_eth.get_address_transactions({
    address: input.address
    ,page : 1
    ,offset : 1
    ,sort : "asc"
  })
    .then((result) => {

      const oldest = result.transactions[0]
      return {
        unixTime: oldest.timeStamp,
        niceTime: timeConverter(oldest.timeStamp)
      }
    })
    .then((result) => {
      if(!result.unixTime){
        return {
          "response_type": "ephemeral",
          "text": `Something went wrong!`
        }
      }

      return {
        "response_type": "in_channel",
        "text": `<!date^${result.unixTime}^First Transaction {date_num} {time_secs}| ${result.niceTime}>`
      }
    })
})


function timeConverter(UNIX_timestamp){
  const a = new Date(UNIX_timestamp * 1000)
  const months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
  const year = a.getFullYear()
  const month = months[a.getMonth()]
  const date = a.getDate()
  const hour = a.getHours()
  const min = a.getMinutes()
  const sec = a.getSeconds()
  return date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec
}