const fdk=require('@fnproject/fdk');
const a8=require('@autom8/js-a8-fdk')
const api=require('etherscan-api').init('notarealkey')

fdk.handle(function(input){
  if (!input.address) {
    return {message: 'please include an account'}
  }
  const balance = api.account.balance(input.address)

  return balance.then((balanceData)=>{
     if(balanceData.status == 1){
       return { ether: balanceData.result/1000000000000000000}
     }
  })
})
