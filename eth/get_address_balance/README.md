# get_address_balance
Gets eth balance of an address

#### example calls:
##### cli
`a8 invoke crypto_eth.get_address_balance '{"address":"0xB8c77482e45F1F44dE1745F52C74426C631bDD52"}'`

## input
`address` is an eth address
```
{
    address : string
}
```

## output
`ether` is the amount of eth currently associated with the address  

```
 { 
    ether: int
 }   
```