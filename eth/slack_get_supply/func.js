const fdk=require('@fnproject/fdk');
const a8=require('@autom8/js-a8-fdk')

fdk.handle(function(input){

  return a8.crypto_eth.get_supply(input)
    .then((result) => {
      if(!result.count){
        return {
          "response_type": "ephemeral",
          "text": `Something went wrong!!`
        }
      }

      return {
        "response_type": "in_channel",
        "text": `Total ETH mined: ${numberWithCommas(result.count)}`
      }
    })
})

const numberWithCommas = (x) => {
  const numSplit = x.toString().split('.')
  numSplit[0] =  numSplit[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",")

  return numSplit.join('.')
}