# slack_get_supply
Gets the count of total eth, formatted for Slack

#### example calls:
##### cli
`a8 invoke crypto_eth.slack_get_supply `

##### slack
`/a8 invoke crypto_eth.slack_get_supply()`

## input
none or 
`{}`

## output
count is number of eth mined up to this point
```
{
  "response_type": "in_channel",
  "text": `Current number of eth mined: ${result.count}`
}   
```