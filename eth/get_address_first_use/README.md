# get_address_first_use_date
Gets the date of the first time an address made a transaction

#### example calls:
##### cli
`a8 invoke crypto_eth.get_address_first_use_date '{"address":"0xB8c77482e45F1F44dE1745F52C74426C631bDD52"}'`

## input
`address` is an eth address
```
{
    address : string
}
```

## output
`unixTime` is the time in seconds since unix epoch.   
`niceTime` is a well formated human readable string , eg `'2 Nov 2018 11:27:44`

```
 { 
    unixTime: int, 
    niceTime: DD Mon YYYY hh:mm:ss 
 }   
```