# get_usd_price
Gets price of an amount of eth in usd and btc

#### example calls:
##### cli
`a8 invoke crypto_eth.get_usd_price`

## input
`wei` and `eth` add together, default `1` eth
```
{
    wei : int,
    eth : int
}
```

## output
`usd` is the amount in usd of the eth   

```
 { 
    priceInUsd: int,  
    priceInBTC: int
 }   
```
