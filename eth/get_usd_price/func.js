const fdk=require('@fnproject/fdk');
const a8=require('@autom8/js-a8-fdk')
const api=require('etherscan-api').init('notarealkey')

const weiToEth = 1000000000000000000

fdk.handle(function(input){
  eth = 0
  
  if (input.wei){
    eth = input.wei/weiToEth
  }
	
  if (input.eth) {
    eth += input.eth 
  }

  if(eth === 0){
    eth = 1  
  }

  return api.stats.ethprice().then((ethPriceResult)=>{
    if(ethPriceResult.status !== '1'){
      return {
        error : "etherscan API failed"
      }
    }

    return {
      priceInUsd : ethPriceResult.result.ethusd*eth
      ,priceInBTC : ethPriceResult.result.ethbtc*eth
    }
  })
})
