const fdk=require('@fnproject/fdk');
const a8=require('@autom8/js-a8-fdk')

fdk.handle(function(input){

  return a8.crypto_eth.get_usd_price(input)
    .then((result) => {
      if(!result.priceInUsd){
        return {
          "response_type": "ephemeral",
          "text": `Couldn't find eth for this account!`
        }
      }

      return {
        "response_type": "in_channel",
        "text": `$${result.priceInUsd} | ${result.priceInBTC} BTC `
      }
    })
})