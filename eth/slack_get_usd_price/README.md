# slack_get_usd_price
Gets eth balance of an address, formatted for Slack

#### example calls:
##### cli
`a8 invoke crypto_eth.slack_get_usd_price `

##### slack
`/a8 invoke crypto_eth.slack_get_usd_price()`

## input
`wei` and `eth` add together, default `1` eth
```
{
    wei : int,
    eth : int
}
```

## output
`ether` is the amount of eth currently associated with the address  

```
{
  "response_type": "in_channel",
  "text": `$${priceInUsd} | ${priceInBTC} btc `
}
```