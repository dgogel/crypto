const fdk=require('@fnproject/fdk');
const a8=require('@autom8/js-a8-fdk')
const api=require('etherscan-api').init('notarealkey')

const weiToGwei = 1000000000

fdk.handle(function(input){

   return api.proxy.eth_gasPrice().then((ethGasPriceResult)=>{

     const gasPriceWei = (parseInt(ethGasPriceResult.result,16))

     return {
      gwei : gasPriceWei/weiToGwei
     }
  })
})
