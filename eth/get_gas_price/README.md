# get_gas_price
Gets the price of gas for Ethereum transactions

#### example calls:
##### cli
`a8 invoke crypto_eth.get_gas_price`

## input
`{}` or none

## output
`gwei` current approximate price of gas in gwei  

```
 { 
    gwei: int
 }   
```