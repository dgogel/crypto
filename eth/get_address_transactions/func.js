const fdk=require('@fnproject/fdk');
const a8=require('@autom8/js-a8-fdk')
const api=require('etherscan-api').init('notarealkey')

fdk.handle(function(input){

  let address = ""
  if (input.address) {
    address = input.address
  }else{
    return {message: 'please include an account'}
  }

  const startblock = input.startblock || 1
  const endblock = input.endblock || 'latest'
  const page = input.page || 1
  const offset = input.offset || 1000
  const sort = input.sort || 'asc'

  return api.account.txlist(address, startblock, endblock, page, offset, sort)
    .then((result)=>{
      return {
        transactions: result.result
      }
  })
})
