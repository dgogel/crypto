const fdk=require('@fnproject/fdk');
const a8=require('@autom8/js-a8-fdk')

fdk.handle(function(input){

  return a8.crypto_eth.get_gas_price(input)
    .then((result) => {
      if(!result.gwei){
        return {
          "response_type": "ephemeral",
          "text": `Something went wrong!!`
        }
      }

      return {
        "response_type": "in_channel",
        "text": `${result.gwei}  gwei`
      }
    })
})