# slack_get_gas_price
Gets the price of gas for Ethereum transactions, formatted for Slack

#### example calls:
##### cli
`a8 invoke crypto_eth.slack_get_gas_price`

##### slack
`/a8 invoke crypto_eth.slack_get_gas_price()`


## input
`{}` or none

## output
`gwei` current approximate price of gas in gwei  

```
{
 "response_type": "in_channel",
 "text": `${gwei}  gwei`
} 
```