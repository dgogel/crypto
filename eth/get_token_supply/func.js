const fdk=require('@fnproject/fdk');
const a8=require('@autom8/js-a8-fdk')
const api=require('etherscan-api').init('notarealkey')

fdk.handle(function(input){
  return api.stats.tokensupply(null, input.address).then((tokenSupplyResult)=>{
    if(tokenSupplyResult.status !== '1'){
     return {
       error : "etherscan API failed"
     }
    }

    return {
      count : tokenSupplyResult.result
    }
  })
})
