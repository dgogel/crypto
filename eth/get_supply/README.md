# get_supply
Gets the count of total eth

#### example calls:
##### cli
`a8 invoke crypto_eth.get_supply`

## input
none or 
`{}`

## output
count is number of eth mined up to this point
```
 {
   count: int
 }   
```