const fdk=require('@fnproject/fdk')
const a8=require('@autom8/js-a8-fdk')
const api=require('etherscan-api').init('notarealkey')

const weiInEither = 1000000000000000000

fdk.handle(function(input){

   return api.stats.ethsupply().then((ethSupplyResult)=>{
    if(ethSupplyResult.status !== '1'){
     return {
       error : "etherscan API failed"
     }
    }

    return {
      count : ethSupplyResult.result /weiInEither
    }
  })
})
