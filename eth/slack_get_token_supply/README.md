# slack_get_token_supply
Gets the total count of an ERC-20 token, formatted for Slack

#### example calls:
##### cli
`a8 invoke crypto_eth.slack_get_token_supply '{"address":"0xB8c77482e45F1F44dE1745F52C74426C631bDD52"}'`

##### slack
`/a8 invoke crypto_eth.slack_get_token_supply(address:0xB8c77482e45F1F44dE1745F52C74426C631bDD52)`

## input
address is the address of a given token
```
 {
   address: string
 }   
```

## output
count is number of tokens currently in existence
```
{
  "response_type": "in_channel",
  "text": `Current number of tokens available: ${count}`
} 
```